chrome.webRequest.onBeforeRequest.addListener(
    function(details) {

      function getParameterByName(url,name) {
          name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
          var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
              results = regex.exec(url);
          return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
      };

      var url = document.createElement('a');
      url.href = details.url;
      var queryString = getParameterByName(url,'q');
        if( details.url.indexOf("+-site:w3schools.com") == -1 && details.url.indexOf('php') != -1)
            return {redirectUrl: url.protocol + url.host + "/search?q=" + queryString +"+-site:w3schools.com" };
    },
    {urls: ["*://*.google.com/search*","*://*.google.co.uk/search*"]},
    ['blocking']
  );
